import java.util.Scanner;

public class Main {

    public static int inputNumber(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the side of the triangle in INT: ");
        int number = scanner.nextInt();
        return number;
    }
    public static void main(String[] args) {

        Triangle triangle = new Triangle();

        System.out.println("Side A is: " + triangle.getSideA()
                + ", Side B: " + triangle.getSideB()
                + ", Side C: "+ triangle.getSideC() + "\n");

        triangle.setSideA(inputNumber());
        triangle.setSideB(inputNumber());
        triangle.setSideC(inputNumber());

        int area = triangle.triangleArea();
        int perimeter = triangle.trianglePerimeter();

        System.out.println("Now your sides are: A is " +
                triangle.getSideA() + ", B is " + triangle.getSideB() + " and C is "+ triangle.getSideC() + "\n");
        System.out.println("The perimeter of this triangle is " + perimeter
                + " and the area is " + area);
    }

}