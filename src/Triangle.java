public class Triangle {
    private int sideA;
    private int sideB;
    private int sideC;

    public Triangle(){
        sideA = 1;
        sideB = 1;
        sideC = 1;
    }

    public int getSideA() {
        return sideA;
    }

    public int getSideB() {
        return sideB;
    }

    public int getSideC() {
        return sideC;
    }

    public void setSideA(int sideA) {
        this.sideA = sideA;
    }

    public void setSideB(int sideB) {
        this.sideB = sideB;
    }

    public void setSideC(int sideC) {
        this.sideC = sideC;
    }

    public int trianglePerimeter(){
        return sideA + sideB + sideC;
    }

    public int triangleArea(){
        int halfPerimeter = trianglePerimeter() / 2;

        int area = (sideA + sideB + sideC) * halfPerimeter;

        return area;
    }


}




